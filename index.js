const express = require('express');
const cors = require('cors');
const puppeteer = require('puppeteer');

const app = express();
app.use(cors());
app.use(express.json());

app.post('/', async (req, res) => {
  const locations = req.body.locations;
  if (locations && Array.isArray(locations)) {
    try {
      const screenshotBuffer = await generateMapScreenshot(locations);
      res.setHeader('Content-Type', 'image/png');
      res.status(200).send(screenshotBuffer);
    } catch (error) {
      console.error('Error generating map screenshot:', error);
      res.status(500).json({ error: 'Failed to generate map screenshot' });
    }
  } else {
    res.status(400).json({ error: 'Locations parameter is missing or invalid' });
  }
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});

async function generateMapScreenshot(locations) {
  try {
    const browser = await puppeteer.launch({ headless: 'shell' });
    const page = await browser.newPage();
    await page.setViewport({ width: 0, height: 0 });

    const center = calculateCenter(locations);
    const zoom = calculateZoom(locations);
    const markers = locations.join('|');
    const mapUrl = `https://maps.googleapis.com/maps/api/staticmap?center=${center}&zoom=${zoom}&size=640x640&markers=${markers}&key=AIzaSyBvivpZTRxbl7MY6aAqhHaDKqK-bp1fnXM`;
    
    console.log("Map URL:", mapUrl); // Print the generated map URL for debugging

    await page.goto(mapUrl, { waitUntil: 'networkidle0' });

    const screenshotBuffer = await page.screenshot({ type: 'png', fullPage: true });
    await browser.close();
    return screenshotBuffer;
  } catch (error) {
    console.error('Error generating map screenshot:', error);
    throw error;
  }
}




function calculateCenter(locations) {
  let latSum = 0;
  let lngSum = 0;
  for (const location of locations) {
    const [lat, lng] = location.split(',').map(parseFloat);
    latSum += lat;
    lngSum += lng;
  }
  const latAvg = latSum / locations.length;
  const lngAvg = lngSum / locations.length;
  return `${latAvg},${lngAvg}`;
}

function calculateZoom(locations) {
  const lats = locations.map(location => parseFloat(location.split(',')[0]));
  const lngs = locations.map(location => parseFloat(location.split(',')[1]));
  const maxLat = Math.max(...lats);
  const minLat = Math.min(...lats);
  const maxLng = Math.max(...lngs);
  const minLng = Math.min(...lngs);

  const latDistance = maxLat - minLat;
  const lngDistance = maxLng - minLng;

  const latZoom = Math.floor(Math.log2(360 / (latDistance * 256)));
  const lngZoom = Math.floor(Math.log2(360 / (lngDistance * 256)));
  return Math.min(latZoom, lngZoom);
}
